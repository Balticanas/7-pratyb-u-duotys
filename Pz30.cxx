#include <TGenPhaseSpace.h>
#include <TH2.h>
#include <TH1.h>
#include <cmath>
#include <TFile.h>
#include <TCanvas.h>

TFile *f30 = new TFile("Pz30.cxx");


void Pz30() {

    int Pz;
    Pz = 30;
    
    TLorentzVector pradinis(0, 0, Pz, sqrt(91.2 * 91.2 + Pz * Pz)); // 4D judesio kiekio vektorius

    Double_t M[2] = { 0.139, 0.139 }; // piono mases

    TGenPhaseSpace event;
    event.SetDecay(pradinis, 2, M); //nustatyti kaip decay'ina dalele

    TH2F* px1py1 = new TH2F("px1py1", "px1 py1", 100, -50, 50, 100, -50, 50); // 2D histograma
    TH2F* px2py2 = new TH2F("px2py2", "px2 py2", 100, -50, 50, 100, -50, 50); // 2D histograma
    TH2F* px1px2 = new TH2F("px1px2", "px1 px2", 100, -50, 50, 100, -50, 50); // 2D histograma
    TH2F* py1py2 = new TH2F("py1py2", "py1 py2", 100, -50, 50, 100, -50, 50); // 2D histograma

    TH1F* E1 = new TH1F("E1", "E1", 50, 30, 65); // 1D histograma
    TH1F* E2 = new TH1F("E2", "E2", 50, 30, 65); // 1D histograma

    for (Int_t n = 0 ; n < 100000 ; n++) 
    {
        Double_t S = event.Generate();

        TLorentzVector* pmiu1 = event.GetDecay(0);

        TLorentzVector* pmiu2 = event.GetDecay(1);

        px1py1->Fill((*pmiu1).Px(), (*pmiu1).Py(), S);//Fill px1py1 array'u uzpildo su S vertemis, nuo *pmiu1).Px() iki  (*pmiu1).Py() nario
        px2py2->Fill((*pmiu2).Px(), (*pmiu2).Py(), S);//
        px1px2->Fill((*pmiu1).Px(), (*pmiu2).Px(), S);//
        py1py2->Fill((*pmiu1).Py(), (*pmiu2).Py(), S);//

        if (n % 1000 == 0) {
            // Surasomi pmiu 4-vektoriaus vertes. Su if'u iseina 100 punktu, o ne 100k
            std::cout << "    miu1:    (";
            for (int i = 0; i<4; i++) {
                std::cout << (*pmiu1)[i] << " ";
            };
            std::cout << ")" << endl;
            std::cout << "    miu2:    (";
            for (int i = 0; i<4; i++) {
                std::cout << (*pmiu2)[i] << " ";
            };
            std::cout << ")" << endl;
            std::cout << "miu1 + miu2: (";
            for (int i = 0; i<4; i++) {
                std::cout << (*pmiu1)[i] + (*pmiu2)[i] << " ";
            };
            std::cout << ")" << endl << endl;
        };

        E1->Fill((*pmiu1).E());
        E2->Fill((*pmiu2).E());
    }

TCanvas *c1 = new TCanvas();
c1->TPad::Divide(2,2);
(*c1).cd(1);
px1py1->Draw("COLZ");
(*c1).cd(2);
px2py2->Draw("COLZ");
(*c1).cd(3);
px1px2->Draw("COLZ");
(*c1).cd(4);
py1py2->Draw("COLZ");
c1->Print("pp30.png");
TCanvas *c2 = new TCanvas();
(*c2).cd(1);
THStack *Eg = new THStack("E", "E");
E1->SetFillColor(kBlack);
Eg->Add(E1);
E2->SetFillColor(kRed);
Eg->Add(E2);
Eg->Draw("nostackb");
c2->Print("E30.png");
}

